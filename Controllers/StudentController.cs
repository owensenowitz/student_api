﻿using Microsoft.AspNetCore.Mvc;
using StudentAPI.Models;
using Blazor.Shared.Models;

namespace BlazorOidcAuthentication.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepository studentRepository;
        public StudentController(IStudentRepository studentRepository)
        {
            this.studentRepository = studentRepository;
        }

        [HttpGet]
        public async Task<ActionResult> GetStudents()
        {
            try
            {
                return Ok(await studentRepository.GetStudents());
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<Student>> GetStudent(int id)
        {
            try
            {
                var result = await studentRepository.GetStudent(id);

                if (result == null)
                {
                    return NotFound();
                }
                return result;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }
        [HttpPost]
        public async Task<ActionResult<Student>> CreateStudent(Student student)
        {
            try
            {
                if (student == null)
                {
                    return BadRequest();
                }

                var createdStudent = await studentRepository.AddStudent(student);

                return CreatedAtAction(nameof(GetStudent), 
                    new { id = createdStudent.Id }, createdStudent);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating new student record");
            }
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult<Student>> UpdateStudent(int id, Student student)
        {
            try
            {
                if (id != student.Id)
                    return BadRequest("Student ID mismatch");

                var studentToUpdate = await studentRepository.GetStudent(id);

                if (studentToUpdate == null)
                {
                    return NotFound($"Student with ID = {id} not found");
                }

                return await studentRepository.UpdateStudent(student);
            }


            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating student record");
            }
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteStudent(int id)
        {
            try
            {
                var studentToDelete = await studentRepository.GetStudent(id);

                if (studentToDelete == null)
                {
                    return NotFound($"Student with ID = {id} not found");
                }

                await studentRepository.DeleteStudent(id);

                return Ok($"Student with ID = {id} deleted");
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleteing student record");
            }
        }

        [HttpGet("{username}")]
        public async Task<ActionResult<IEnumerable<Student>>> Search(string username)
        {
            try
            {
                var result = await studentRepository.Search(username);

                if (result.Any())
                {
                    return Ok(result);
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }
    }
}
