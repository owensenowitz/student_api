﻿using Blazor.Shared.Models;
namespace StudentAPI.Models
{
    public interface IStudentRepository
    {
        Task<IEnumerable<Student>> GetStudents();
        Task<Student> GetStudent(int id);
        Task<Student> AddStudent(Student student);
        Task<Student> UpdateStudent(Student student);
        Task<Student> GetStudentByEmail(string email);
        Task DeleteStudent(int id);
        Task<IEnumerable<Student>> Search(string username);
    }
}
