﻿using Blazor.Shared.Models;
using Microsoft.EntityFrameworkCore;
using StudentAPI.Models;

namespace StudentAPI.Models
{
    public class StudentRepository : IStudentRepository
    {
        private readonly AppDbContext appDbContext;
        public StudentRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }
        public async Task<Student> AddStudent(Student student)
        {
            var result = await appDbContext.Students.AddAsync(student);
            await appDbContext.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<IEnumerable<Student>> Search(string username)
        {
            IQueryable<Student> query = appDbContext.Students;

            if (!string.IsNullOrEmpty(username))
            {
                query = query.Where(x => x.UserName.Contains(username));
            }
            return await query.ToListAsync();
        }

        public async Task<IEnumerable<Student>> GetStudents()
        {
            return await appDbContext.Students.ToListAsync();
        }

        public async Task<Student> UpdateStudent(Student student)
        {
            var result = await appDbContext.Students
                .FirstOrDefaultAsync(x => x.Id == student.Id);

            if (result != null)
            {
                result.UserName = student.UserName;
                result.Email = student.Email;
                result.PasswordHash = student.PasswordHash;
                result.PhoneNumber = student.PhoneNumber;
                result.Major = student.Major;
                result.Class1 = student.Class1;
                result.Class2 = student.Class2;
                result.Class3 = student.Class3;
                result.Class4 = student.Class4;
                result.Class5 = student.Class5;
                result.Class6 = student.Class6;

                await appDbContext.SaveChangesAsync();

                return result;
            }
            return null;
        }

        public async Task<Student> GetStudent(int id)
        {
            return await appDbContext.Students
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task DeleteStudent(int id)
        {
            var result = await appDbContext.Students
                .FirstOrDefaultAsync(x => x.Id == id);

            if (result != null)
            {
                appDbContext.Students.Remove(result);
                await appDbContext.SaveChangesAsync();
            }
        }

        public async Task<Student> GetStudentByEmail(string email)
        {
            return await appDbContext.Students
                .FirstOrDefaultAsync(x => x.Email == email);
        }
    }
}
